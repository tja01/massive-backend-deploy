import os
import signal
import logging
import time
import pykka

import tornado.auth
import tornado.escape
import tornado.httpserver
import tornado.options
import tornado.web
import tornado.autoreload
from tornado.options import options, parse_command_line
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.concurrent

from oscarbao.api.ML.cognition_handler import CognitionFitHandler, CognitionPredictHandler
from oscarbao.api.ML.decision_tree_handler import DecisionTreeFitHandler, DecisionTreePredictHandler
from oscarbao.api.ML.regression_handlers import RegressionFitHandler, RegressionPredictHandler
from oscarbao.api.ML.classification_handlers import ClassificationFitHandler, ClassificationPredictHandler
from oscarbao.api.ML.random_forest_handler import RandomForestFitHandler, RandomForestPredictHandler
from oscarbao.api.ML.clustering_handler import ClusteringFitHandler, ClusteringPredictHandler, ClusteringFitAndPredictHandler
from oscarbao.api.ML.selection_state_handlers import SelectionStateHandler
from oscarbao.api.ML.timeseries_handler import TimeseriesFitHandler
from oscarbao.api.misc.expression_builder_handlers import ExpressionBuilderHandler
from oscarbao.api.misc.datasets_handler import DatasetsHandler
from oscarbao.api.misc.transform_handlers import TransformHandler
from oscarbao.api.ML.sentiment_handler import SentimentFitHandler, SentimentPredictHandler
from oscarbao.api.ML.recommender_handlers import RecommenderFitHandler, RecommenderPredictHandler
from oscarbao.api.misc.query_handlers import QueryHandler
from oscarbao.api.misc.stats_handlers import StatsHandler
from oscarbao.api.misc.bin_handler import BinHandler
from oscarbao.api.misc.chord_chart_handler import ChordHandler
from oscarbao.data_stream.import_handler import ImportHandler
from oscarbao.data_stream.preview_handler import PreviewHandler, PreviewParserHandler
from oscarbao.api.misc.cancel_jobs_handler import CancelJobsHandler
from oscarbao.data_stream.upload_jar_handler import UploadJarHandler
from oscarbao.data_stream.delete_jar_handler import DeleteJarHandler
from oscarbao.api.misc.java_version_handler import JavaVersionHandler
from oscarbao.api.misc.join_handler import JoinHandler, JoinSanityCheckHandler
from oscarbao.utils.java_gateway import launch_gateway
from oscarbao.api.oscar_context import OscarContext
from oscarbao.utils import helper


tornado.options.define("env", type=str)

class Application(tornado.web.Application):

    def __init__(self, hdfs_host, env, autoreload):

        handlers = [
                (r"/regressionfit", RegressionFitHandler, dict(hdfs_host=hdfs_host,
                                                               env=env)),
                (r"/regressionpredict", RegressionPredictHandler, dict(hdfs_host=hdfs_host,
                                                                       env=env)),
                (r"/classificationfit", ClassificationFitHandler, dict(hdfs_host=hdfs_host,
                                                                       env=env)),
                (r"/classificationpredict", ClassificationPredictHandler, dict(hdfs_host=hdfs_host,
                                                                               env=env)),
                (r"/cognitionfit", CognitionFitHandler, dict(hdfs_host=hdfs_host,
                                                         env=env)),
                (r"/cognitionpredict", CognitionPredictHandler, dict(hdfs_host=hdfs_host,
                                                                 env=env)),
                (r"/rffit", RandomForestFitHandler, dict(hdfs_host=hdfs_host,
                                                                 env=env)),
                (r"/rfpredict", RandomForestPredictHandler, dict(hdfs_host=hdfs_host,
                                                                 env=env)),
                (r"/decisiontreefit", DecisionTreeFitHandler, dict(hdfs_host=hdfs_host,
                                                         env=env)),
                (r"/decisiontreepredict", DecisionTreePredictHandler, dict(hdfs_host=hdfs_host,
                                                                 env=env)),
                (r"/clusteringfit", ClusteringFitHandler, dict(hdfs_host=hdfs_host,
                                                         env=env)),
                (r"/clusteringfitandpredict", ClusteringFitAndPredictHandler, dict(hdfs_host=hdfs_host,
                                                         env=env)),
                (r"/clusteringpredict", ClusteringPredictHandler, dict(hdfs_host=hdfs_host,
                                                                 env=env)),
                (r"/tsfit", TimeseriesFitHandler, dict(hdfs_host=hdfs_host,
                                                             env=env)),
                (r"/query", QueryHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/stats", StatsHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/bin", BinHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/chord", ChordHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/expressionbuilder", ExpressionBuilderHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/sentimentfit", SentimentFitHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/sentimentpredict", SentimentPredictHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/recommenderfit", RecommenderFitHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/recommenderpredict", RecommenderPredictHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/importdata", ImportHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/preview", PreviewHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/previewparsing", PreviewParserHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/transform", TransformHandler, dict(hdfs_host=hdfs_host,
                                               env=env)),
                (r"/selectionstate", SelectionStateHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/canceljob", CancelJobsHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/datasets/(.*)", DatasetsHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/uploadjar/(.*)", UploadJarHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/deletejar", DeleteJarHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/javaversion", JavaVersionHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/join", JoinHandler, dict(hdfs_host=hdfs_host, env=env)),
                (r"/joincheck", JoinSanityCheckHandler, dict(hdfs_host=hdfs_host, env=env))
        ]

        template_path = "templates"

        if autoreload:
            debug = True
            for (path, dirs, files) in os.walk(template_path):
                for item in files:
                    tornado.autoreload.watch(os.path.join(path, item))
        else:
            debug = False

        settings = dict(template_path=template_path,
                        static_path="static",
                        debug=debug)

        tornado.web.Application.__init__(self, handlers, **settings)

def sig_handler(sig, frame):
    logging.warning('Caught signal: %s', sig)
    tornado.ioloop.IOLoop.instance().add_callback(shutdown)

def shutdown():

    MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 5

    logging.info('Stopping http server')
    http_server.stop()

    logging.info('Will shutdown in %s seconds ...', MAX_WAIT_SECONDS_BEFORE_SHUTDOWN)
    io_loop = tornado.ioloop.IOLoop.instance()

    deadline = time.time() + MAX_WAIT_SECONDS_BEFORE_SHUTDOWN

    def stop_loop():
        now = time.time()
        if now < deadline and (io_loop._callbacks or io_loop._timeouts):
            io_loop.add_timeout(now + 1, stop_loop)
        else:
            if OscarContext._ssc is not None:
                OscarContext._ssc.stop(True, False)
            else:
                OscarContext._sc.stop()
            pykka.ActorRegistry.stop_all()
            io_loop.stop()
            logging.info('Shutdown')
    stop_loop()

def main(env, isTest):
    logging.root.setLevel(logging.INFO)

    OscarContext(isTest, env)

    config = helper.get_config()
    autoreload = config.get(env).get('tornado_autoreload')

    global http_server

    if not isTest:
        tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application(config.get(env).get('hdfs_host'), env, autoreload))
    http_server.listen(config.get(env).get("spark_tornado_port"))

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    parse_command_line()
    env = options.env
    main(env, False)
