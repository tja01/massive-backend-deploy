#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied"
  else
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    echo $DIR
    $DIR/stop_spark.sh
    sleep 10
    $DIR/start_spark.sh $1
fi

