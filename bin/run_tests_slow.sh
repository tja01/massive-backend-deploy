#!/bin/bash

cd $MASSIVE_HOME/oscarbao/tests/web_tests
py.test -s --junitxml=$MASSIVE_HOME/web_tests_report.xml $1

cd $MASSIVE_HOME/oscarbao/tests/api_tests
py.test --runslow -s --junitxml=$MASSIVE_HOME/api_tests_report.xml $1
