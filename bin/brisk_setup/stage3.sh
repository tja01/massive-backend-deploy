#!/bin/bash

#additional dependencies
sudo apt-get -y install git gfortran libblas-dev liblapack-dev python-numpy python-scipy python-pandas python-scikits-learn python-nltk &
ssh brisk1 'sudo apt-get -y install gfortran libblas-dev liblapack-dev python-numpy python-scipy python-pandas python-scikits-learn python-nltk' &
ssh brisk2 'sudo apt-get -y install gfortran libblas-dev liblapack-dev python-numpy python-scipy python-pandas python-scikits-learn python-nltk' &

wait
echo "Install packages is completed"

#get latest pip (bug)
sudo easy_install -U pip &
ssh brisk1 'sudo easy_install -U pip' &
ssh brisk2 'sudo easy_install -U pip' &

#create folders
sudo mkdir -p /mnt/tmp/massive/
sudo chown briskuser:briskuser /mnt/tmp/massive/
sudo mkdir -p /mnt/tmp/spark-events/
sudo chown briskuser:briskuser /mnt/tmp/spark-events/

#add path
SPARK_HOME=/usr/spark
MASSIVE_HOME=~/massiveanalytic
echo 'export SPARK_HOME='$SPARK_HOME >> ~/.bashrc
echo 'export PYTHONPATH=$SPARK_HOME/python/lib/py4j-0.8.2.1-src.zip:$SPARK_HOME/python' >> ~/.bashrc
echo 'export MASSIVE_HOME='$MASSIVE_HOME >> ~/.bashrc

wait
echo "Created directory structure"

#build python packages
cd $MASSIVE_HOME
python setup.py bdist_egg
sudo easy_install dist/OscarBao-0.5.0-py2.7.egg

scp $MASSIVE_HOME/dist/OscarBao-0.5.0-py2.7.egg brisk1:~/
scp $MASSIVE_HOME/dist/OscarBao-0.5.0-py2.7.egg brisk2:~/

ssh brisk1 'sudo easy_install ~/OscarBao-0.5.0-py2.7.egg' &
ssh brisk2 'sudo easy_install ~/OscarBao-0.5.0-py2.7.egg' &

wait
echo "Python dependencies setup"

#TODO modify oscar config (fill in frontend api address)
#TODO modify hive-site.xml to setup the correct metastore server (sql metastore address)

#setup SQL server connection
cp $MASSIVE_HOME/bin/brisk_setup/hive-site.xml $SPARK_HOME/conf
echo 'SPARK_CLASSPATH=$SPARK_CLASSPATH:$MASSIVE_HOME/sqldrivers/mysql-connector-java-5.1.34-bin.jar:$SPARK_HOME/conf' | sudo tee -a $SPARK_HOME/conf/spark-env.sh
echo 'SPARK_CLASSPATH=$SPARK_CLASSPATH:$MASSIVE_HOME/sqldrivers/commons-dbcp-1.4.jar:$MASSIVE_HOME/sqldrivers/commons-pool-1.6.jar' | sudo tee -a $SPARK_HOME/conf/spark-env.sh

#TODO open port for spark web on brisk (60030)
#TODO open streaming port on brisk (5555)

#startup massive backend API
$MASSIVE_HOME/bin/start_spark.sh --env=brisk


