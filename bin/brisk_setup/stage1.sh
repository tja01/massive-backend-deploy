#!/bin/bash

MASSIVE_FRONTEND=~/massive-analytic-front-end/
MASSIVE_HOME=~/massiveanalytic/
echo 'export MASSIVE_FRONTEND='$MASSIVE_FRONTEND >> ~/.bashrc
echo 'export MASSIVE_HOME='$MASSIVE_HOME >> ~/.bashrc

sudo apt-get install -y git tmux mysql-server python-dev build-essential python-pip

#checkout repos
git clone -b streaming https://tja01@bitbucket.org/massiveanalytic/massive-analytic-front-end.git
git clone -b develop https://tja01@bitbucket.org/hjmeadows/massiveanalytic.git

#install node and npm
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs

#install mongo
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

#set dbpath in /etc/mongod.conf to /datadrive/mongodb

#install sbt
sudo apt-get purge sbt
wget http://dl.bintray.com/sbt/debian/sbt-0.13.5.deb -P /tmp/
sudo dpkg -i /tmp/sbt-0.13.5.deb
sudo apt-get update
sudo apt-get install sbt

#build scala packages
cd $MASSIVE_HOME/randomforest_scala
sbt package
sbt assembly

#TODO fill in frontend config
#TODO open port for sql server (3306) and frontend (8080)

#TODO permission to access mysql server remotely and initial setup
# Open up the file under 'etc/mysql/my.cnf'
# Change datadir to /datadrive/mysql (check the exact mechanism for this)
# Comment out bind-address = 127.0.0.1
# wait_timeout = 60 #close down opened connections
# GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'test' WITH GRANT OPTION;
# SET PASSWORD FOR 'root'@'%' = PASSWORD('test');
# FLUSH PRIVILEGES;
# CREATE DATABASE metastore;
# add 137.116.197.205 brisktest11.cloudapp.net to /etc/hosts (reverse lookup)

cp $MASSIVE_HOME/brisk_setup/node_check.sh

# add to crontab
# *  * * * * root /home/azureuser/node_check.sh &

#azure command line interface
sudo npm install azure-cli -g

#note read VM info from /var/lib/waagent/SharedConfig.xml

#deploy frontend
cd $MASSIVE_FRONTEND
npm update
NODE_ENV=local node app &