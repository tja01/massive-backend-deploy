__author__ = 'tja01'

sc = None

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.jsonFile("file:///usr/spark/examples/src/main/resources/people.json")

for i in range(1, 10):
    df = df.unionAll(df)

#%timeit df.collect()

from pyspark.streaming import StreamingContext
ssc = StreamingContext(sc, 15)
rdds = [sc.parallelize(range(5)) for _ in range(100)]
stream = ssc.queueStream(rdds)
stream.pprint()
ssc.start()

#export SPARK_WORKER_MEMORY="1g"
#export SPARK_EXECUTOR_MEMORY="512m"
#export SPARK_DRIVER_MEMORY="1g"
#export SPARK_REPL_MEM="2g"
#export SPARK_WORKER_PORT=9000
#export SPARK_CONF_DIR="/usr/local/spark/conf"
#export SPARK_TMP_DIR="/srv/spark/tmp"
#export SPARK_PID_DIR="/srv/spark/pids"
#export SPARK_LOG_DIR="/srv/spark/logs"
#export SPARK_WORKER_DIR="/srv/spark/work"
#export SPARK_LOCAL_DIRS="/srv/spark/tmp"
#export SPARK_COMMON_OPTS="$SPARK_COMMON_OPTS -Dspark.kryoserializer.buffer.mb=32 "
#LOG4J="-Dlog4j.configuration=file://$SPARK_CONF_DIR/log4j.properties"
#export SPARK_MASTER_OPTS=" $LOG4J -Dspark.log.file=/srv/spark/logs/master.log "
#export SPARK_WORKER_OPTS=" $LOG4J -Dspark.log.file=/srv/spark/logs/worker.log "
#export SPARK_EXECUTOR_OPTS=" $LOG4J -Djava.io.tmpdir=/srv/spark/tmp/executor "
#export SPARK_REPL_OPTS=" -Djava.io.tmpdir=/srv/spark/tmp/repl/\$USER "
#export SPARK_APP_OPTS=" -Djava.io.tmpdir=/srv/spark/tmp/app/\$USER "
#export PYSPARK_PYTHON="/usr/bin/python"
#export SPARK_WORKER_INSTANCES="3"
