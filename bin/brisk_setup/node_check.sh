#!/bin/bash

SERVICE=node
if ps ax | grep -v grep | grep -v $0 | grep $SERVICE > /dev/null
then
    echo "$SERVICE is running - everything is fine"
else
    echo "$SERVICE is not running - restarting..."
    logrotate --force /home/azureuser/rotateConfig
    cd /home/azureuser/massive-analytic-front-end
    NODE_ENV=local nohup node app > /var/log/nodejs.log
fi
