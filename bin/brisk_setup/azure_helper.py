import requests
import json

__author__ = 'tja01'


class BriskAPI(object):

    def __init__(self, o_auth, api_key, bearer):
        self.headers = {'Content-Type': 'application/json',
                        'X-Originator-Auth': o_auth,
                        'Authorization': bearer,
                        'Ocp-Apim-Subscription-Key': api_key}
        self.vm_sizes = {0: "ExtraSmall", 1: "Small", 2: "Medium", 3: "Large",
                         4: "ExtraLarge", 5: "A5", 6: "A6", 7: "A7", 8: "A8", 9: "A9",
                         10: "Basic_A0", 11: "Basic_A1", 12: "Basic_A2", 13: "Basic_A3",
                         14: "Basic_A4", 15: "Standard_D1", 16: "Standard_D2",
                         17: "Standard_D3", 18: "Standard_D4", 19: "Standard_D11",
                         20: "Standard_D12", 21: "Standard_D13", 22: "Standard_D14",
                         23: "Standard_DS1", 24: "Standard_DS2", 25: "Standard_DS3",
                         26: "Standard_DS4", 27: "Standard_DS11", 28: "Standard_DS12",
                         29: "Standard_DS13", 30: "Standard_DS14", 31: "Standard_G1",
                         32: "Standard_G2", 33: "Standard_G3", 34: "Standard_G4",
                         35: "Standard_G5"}

    def execute_command(self, cluster_id, commands, password):
        url = "https://elastacloud.azure-api.net/brisk/v1/cluster/%s/execute" % str(cluster_id)
        data = {
            "Commands": commands,
            "Password": password
        }
        r_post = requests.post(url, headers=self.headers, data=json.dumps(data))
        return r_post.text

    def create_cluster(self, cluster_name, username, password):
        url = "https://elastacloud.azure-api.net/brisk/v1/cluster/"
        new_cluster = {
        "ClusterName": cluster_name,
        "Location": "West Europe",
        "Username": username,
        "Password": password,
        "DeploymentId": 1,
        "AzureVm": 4,
        "NodeCount": 3,
        "DefaultStorageAccount": "brisktest1",
        "DefaultStorageContainer": "privatecontainer",
        "SecondaryStorageAccounts": [],
        "SubscriptionId": "00aee009-2c3d-4314-bfa3-e8c3f5a4ff67",
        "VirtualNetworkRequired": True,
        "VirtualNetworkName": "Group Group-1 oscar2",
        "VirtualNetworkAddressSpace": "10.1.0.0/16"}
        r_post = requests.post(url, headers=self.headers, data=json.dumps(new_cluster))
        return r_post.text

    def get_cluster_info(self):
        url = "https://elastacloud.azure-api.net/brisk/v1/cluster/"
        #read cluster data
        r_get = requests.get(url, headers=self.headers)
        return r_get.text


    def create_backend_config(self, cluster_id, frontend_host, oscar_backend_version="0.5.0"):
        url = "https://elastacloud.azure-api.net/brisk/v1/cluster/%s" % cluster_id
        #read cluster data
        r_get = requests.get(url, headers=self.headers)
        if r_get.ok:
            j = json.loads(r_get.text)
            print j
            config_data = {
                "version": oscar_backend_version,
                "brisk":
                    {
                      "hdfs_host": "",
                      "spark_master": "spark://brisk0:7077",
                      "spark_tornado_port": 60030,
                      "spark_executor_instances": j['NodeCount'],
                      "spark_executor_cores": j['NumberOfCores'] / j['NodeCount'],
                      "spark_executor_memory": "%dG" % (j['RamPerNode'] - 1),
                      "spark_events_dir": "file:///mnt/tmp/spark-events",
                      "datacleansing_metadata": "http://%s:8080/datacleansing" % frontend_host,
                      "datasets": "http://%s:8080/datasets" % frontend_host,
                      "model_metadata": "http://%s:8080/modelsmeta" % frontend_host,
                      "tornado_autoreload": False,
                      "stream_limit": 7,
                      "streaming_storage_level": "MEMORY_AND_DISK_2",
                      "import_streams": True,
                      "stream_duration": 15}
            }
            with open('config.json', 'w') as outfile:
                json.dump(config_data, outfile)
        else:
            raise Exception("Cannot obtain cluster details")