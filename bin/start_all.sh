#! /bin/bash

#start the hadoop cluster
start-dfs.sh
#start spark services
$SPARK_HOME/sbin/start-all.sh
cd $MASSIVE_HOME
./bin/start_spark.sh --env=local_laptop
#start frontend processes
cd /home/massive/massive-analytic-front-end
NODE_ENV=local node app &
