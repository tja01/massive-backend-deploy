#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied"
    cd $SPARK_HOME
  else
    cd $1
fi

export MAVEN_OPTS="-Xmx2g -XX:MaxPermSize=512M -XX:ReservedCodeCacheSize=512m"

mvn -Pdeb -Pyarn -Phadoop-2.3 -Dhadoop.version=2.3.0 -Phive -DskipTests clean package

sbt/sbt publish-local