#! /bin/bash

#start spark services
cd $MASSIVE_HOME
./bin/stop_spark.sh
#stop spark
$SPARK_HOME/sbin/stop-all.sh
#stop hdfs
stop-dfs.sh
